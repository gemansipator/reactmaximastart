import './listItem.scss';

function ListItem({ img, title, price }) {
    return (
        <div className={'ListItem'}>
            <div className={'ListItem__img'}>
                <img src={img}/>
            </div>
            <h2 className={'ListItem__title'}>{title}</h2>
            <div className={'ListItem__price'}>{price}</div>
            <button className={'ListItem__button'}>Добавить в корзину</button>
        </div>
    )

}
export default ListItem;