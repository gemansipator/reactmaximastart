import './List.scss'
import '../listItem/listItem.js';
import ListItem from "../listItem/listItem.js";

function List({list = []}) {
    return (
        <div className={'List'}>
            {
                list.map((item)=> {
                    return <ListItem key={item.id} img = {item.image} title={item.title} price={item.price}></ListItem>
                })
            }

        </div>
    )

}
export default List;

